var
    gulp  = require("gulp"),
    livereload = require("gulp-livereload"),
    browserSync = require("browser-sync");

gulp.task('css',function(){
    return gulp.src('src/*.css')
        .pipe(browserSync.reload({stream: true}))
});

gulp.task('browser-sync', function(){
    browserSync({
        server: {
            baseDir: './src/',
            serveStatic: './src/',
            index: 'index.html'
        },
        notify: false
    });
});

gulp.task('watch', function(){
    gulp.watch('src/*.css',gulp.parallel('css'));
});

gulp.task("reload-css", function() {
    gulp.src('./src/*.css')
        .pipe(livereload());
});
gulp.task("reload-html", function() {
    gulp.src('./src/*.html')
        .pipe(livereload());
});

gulp.task('default', gulp.parallel('css','browser-sync', 'watch'));

/*
gulp.task("default", function() {
    livereload.listen();
    gulp.watch('./src/*.css', gulp.parallel('reload-css', 'default'));
    gulp.watch('./src/*.html', gulp.parallel('reload-html', 'default'));
});*/